# nis-to-idm

Ansible scripts to migrate RHEL 6 and RHEL 7 from a nis domain to a IdM domain

Right now this is just a straight up playbook but it should be converted to roles and modules.

This playbook assumes that:
  1. the client is already in DNS and that hostname is set correctly
  2. there is an ntp source available

More information to follow
